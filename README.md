# Rest-API-Test

A Test project for GitLab CI

## Quick start

```
$ pip install -r requirements.txt
$ . ./run.sh
```

## use Docker image

```
docker run -d -p 15000:5000 registry.gitlab.com/kuwabatak/rest-api-test-server
```

or use docker-compose

```
$ cd ./docker
$ docker-compose up -d
```

access to http://localhost:15000/api/person

